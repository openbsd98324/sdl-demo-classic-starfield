
// SDL  Starfield on FB DEV

/*
all:
	  cc  -lSDL starfield.c  -o starfield 
*/




#include <stdio.h>
#include <SDL/SDL.h>
#define NUM_DOTS 500
#define WINDOW_TITLE "Starfield"
// on notebook
//#define SCREEN_WIDTH 	1366
//#define SCREEN_HEIGHT	768

// 1824 984
#define SCREEN_WIDTH 	1824
#define SCREEN_HEIGHT	984


struct dot
{
	int red,green,blue,speed;
	float vx, vy;
	float x,y;	
};

struct dot demo_dots[NUM_DOTS];

SDL_Surface *main_screen;

void init_dots() {
	int i = 0;
	for (i;i < NUM_DOTS; i++) {
	  demo_dots[i].red = 0;
	  demo_dots[i].green = rand() % 255;
	  demo_dots[i].blue = rand() % 255;
	  demo_dots[i].x = rand() % SCREEN_WIDTH;
	  demo_dots[i].y = rand() % SCREEN_HEIGHT;
	  demo_dots[i].speed = 1 + rand() % 5;
	}
}






void move_dots() {
	int i = 0;
	for (i; i < NUM_DOTS; i ++) {
		demo_dots[i].x = ((int)(demo_dots[i].x + demo_dots[i].speed) % SCREEN_WIDTH );
	}
}








void draw_screen() 
{
	int i=0,bpp,rank,x,y;
	Uint32 *pixel;
	rank = main_screen->pitch/sizeof(Uint32);
	pixel = (Uint32*)main_screen->pixels;

	SDL_LockSurface(main_screen);
	for(i=0; i < NUM_DOTS ; i++) {
		x = (int)demo_dots[i].x;
		y = (int)demo_dots[i].y;
		pixel[x+y*rank] = SDL_MapRGBA(
					main_screen->format,
					demo_dots[i].red,
					demo_dots[i].green,
					demo_dots[i].blue,
					255
		);
	}
	SDL_UnlockSurface(main_screen);
}






int main(int argn, char **argv) 
{

	putenv((char*)"FRAMEBUFFER=/dev/fb0");                                      
	putenv((char*)"SDL_FBDEV=/dev/fb0");                                        


	int active;
	int gFrames = 0;

	SDL_Event event;

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		fprintf(stderr, "Could not initialise %s\n",SDL_GetError());
	}

        main_screen = SDL_SetVideoMode(
		SCREEN_WIDTH,
		SCREEN_HEIGHT, 0,
		SDL_HWSURFACE|SDL_DOUBLEBUF | SDL_FULLSCREEN
	);

	if (!main_screen) {
		fprintf(stderr, "Could not initialise %s\n", SDL_GetError());
	}

	SDL_WM_SetCaption( WINDOW_TITLE, 0 );

	printf("SDL On Line\n");
	init_dots();	

	active = 1;
	while( active == 1 ) 
	{
             SDL_FillRect(main_screen,NULL,SDL_MapRGBA(main_screen->format,0,0,0,255));
	     draw_screen();
             SDL_Flip(main_screen);
	     move_dots();

             while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {

                        // Escape forces us to quit the app
                        case SDLK_ESCAPE:
                        case SDLK_q:
                        case SDLK_F4:
                            event.type = SDL_QUIT;
		            active = 0;
                        break;
                     }

		}
	     }
        }

	SDL_Quit();
	return 0;
}



